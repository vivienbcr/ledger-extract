# Extract secret key information from Ledger device

> This is for Ed25519 and Secp256k1 keys.

A web app can be accessed at https://dune.network/ledger_extract/ .

The SHA256 checksum for the offline version (`ledger_extract.html`) is

<!-- sha256 start -->
    dc6bbed859d674c2da620fe0f5550a77331127531ad55e818945c716a3854497
<!-- sha256 end -->

## Building

### Building the Library

Install `nodejs` and `npm` on your system, then run:

```
npm install
```

### Building the standalone Web page 

You will need some additional dependencies which you can install with

```
npm install --only=dev
```

You can now run:

```
make
```

The standalone html file for offline use will be produced in `www/ledger_extract.html`.

## Usage

### As a Standalone Executable

```
> ./extract \
    --mnemonic "history salmon age blast grit main donate claw sunny trash imitate genre more model enemy" \
    --path "m/44'/1729'/0'/0'" \
    --curve ed25519

Using derivation path m/44'/1729'/0'/0'
Secret Key:      edsk3YPejH8pWWGmYTUsaMFiPMEhQPSx5fpDj42a8LdrTYmtMJEeo2
Public Key:      edpktjHFXSmNigbekQUmiffJo347pq7FQZUJEHtyBMvCmvXRswPp6z
Public Key Hash: dn1YYxZGQks8CmU9gL2FLdh3ErzRK5H5LYfj

```

```
Options:
  --version       Show version number                                  [boolean]
  --mnemonic, -m  BIP39 mnemonic                             [string] [required]
  --path, -p      BIP32 derivation path  [string] [default: "m/44'/1729'/0'/0'"]
  --curve, -c     Cryptographic curve
                 [string] [choices: "ed25519", "secp256k1"] [default: "ed25519"]
  --password, -P  Optional password for mnemonic                        [string]
  --help, -h      Show help                                            [boolean]
```

### As a Javascript Library

```js
const extract = require('ledger_extract').extract

let info = extract("history salmon age blast grit main donate claw sunny trash imitate genre more model enemy", "m/44'/1729'/0'/0', "ed25519") ;
  
console.log(info)
```

This will display:

```js
{ secretKey:
   Uint8Array [ 114, 164, 79, 62, 142, 41, 90, 77, 98, 20, 114, 172, 109, 197, 73, 165, 145, 100, 1, 78, 175, 197, 3, 37, 66, 124, 25, 122, 57, 54, 65, 112 ],
  keyPair:
   { publicKey:
      Uint8Array [ 11, 123, 156, 255, 51, 5, 141, 248, 0, 27, 12, 167, 104, 25, 54, 158, 17, 178, 65, 151, 120, 165, 226, 44, 104, 87, 168, 157, 42, 128, 132, 86 ],
     secretKey:
      Uint8Array [114, 164, 79, 62, 142, 41, 90, 77, 98, 20, 114, 172, 109, 197, 73, 165, 145, 100, 1, 78, 175, 197, 3, 37, 66, 124, 25, 122, 57, 54, 65, 112, 11, 123, 156, 255, 51, 5, 141, 248, 0, 27, 12, 167, 104, 25, 54, 158, 17, 178, 65, 151, 120, 165, 226, 44, 104, 87, 168, 157, 42, 128, 132, 86 ] },
  publicKeyHash:
   Uint8Array [ 180, 13, 152, 246, 150, 242, 25, 14, 4, 84, 214, 58, 181, 249, 92, 183, 245, 33, 146, 250 ],
  base58:
   { secretKey: 'edsk3YPejH8pWWGmYTUsaMFiPMEhQPSx5fpDj42a8LdrTYmtMJEeo2',
     extendedSecretKey:
      'edskRs1ggdiWZneJxUXsJjYueDfjbQsbRfkUzKeRQGtC8QmwqSasKKAvnQeuFtyVU1zyoPBz53D9vCZmVsJst8FkyrT4TDwZPx',
     publicKey: 'edpktjHFXSmNigbekQUmiffJo347pq7FQZUJEHtyBMvCmvXRswPp6z',
     publicKeyHash:
      { dune: 'dn1YYxZGQks8CmU9gL2FLdh3ErzRK5H5LYfj',
        tezos: 'tz1c44cRLWtVqfvuHobXGwZRAuUAVZFrHRBj' } } }
```

## Limitations

Only supports hardened derivation paths for Ed25519 derivations.
