all: node_modules
	mkdir -p www
	cp -fp static/* www/
	node_modules/browserify/bin/cmd.js src/app.js -o www/app.js
	node_modules/html-inline/bin/cmd.js -b www -i www/index.html -o www/ledger_extract.html
	sed -i -e "/<\!-- sha256 start -->/,/<\!-- sha256 end -->/c\<\!-- sha256 start -->\n    $$(sha256sum www/ledger_extract.html | cut -d ' ' -f 1)\n<\!-- sha256 end -->" Readme.md

node_modules:
	npm install --only=dev

publish: all
	rsync -azuv --no-group www/. dune.network:/home/www.dune.network/www/ledger_extract/.
